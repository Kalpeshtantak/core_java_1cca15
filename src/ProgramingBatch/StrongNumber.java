package ProgramingBatch;

public class StrongNumber {
    public static void main(String[] args) {
        for (int a = 1; a <= 1000; a++) {
            //int a = 145;
            int sum = 0;
            int temp = a;
            int rem=0;
            while (temp != 0) {
                 rem = temp % 10;
                int fact = 1;
                for (int i = 1; i <= rem; i++) {
                    fact = fact * i;
                }
                sum = sum + fact;
                 temp/= 10;
            }
            if (sum == a) {
                System.out.println(sum);
            }
        }
    }
}

