package ProgramingBatch;

public class Pattern1 {
    public static void main(String[] args) {

        int star=1;
        int line=5;
        int space =line-1;
        for (int i=0;i<line;i++)
        {
            int no=1;
            for (int k = 0; k < space; k++)
            {
                System.out.print(" ");

            }space--;
            for (int j = 0; j < star; j++)
            {
                System.out.print(no++);
            }
            System.out.println();
            star++;
        }
    }
}
