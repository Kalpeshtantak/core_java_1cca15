package ProgramingBatch;

public class MissingNo {
    public static void main(String[] args) {
        int[] arr ={1,2,3,4,5,6,7,8,10,11};

        int n=arr.length;
        int sum=((n+1)*(n+2))/2;

        for (int a:arr ){
            sum-=a;
        }
        System.out.println("Missing ="+sum);
        System.out.println(arr.length);
    }
}
