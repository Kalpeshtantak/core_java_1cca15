package PatternPrograme;

public class Pattern6 {
    public static void main(String[] args) {
        int lines = 5;
        int stars = 5;
        for (int i = 0; i < lines; i++) {
            int ch=1;
            for (int j = 0; j < stars; j++) {
                System.out.print(" " + ch + " ");
                ch++;
            }
            System.out.println();
        }
    }
}

