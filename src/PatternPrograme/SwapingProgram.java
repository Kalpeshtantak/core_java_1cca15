package PatternPrograme;

import java.util.Scanner;

public class SwapingProgram {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter 1st No");
        int s1= sc.nextInt();
        System.out.println("Enter 2nd No");
        int s2= sc.nextInt();
        int s3=s1;
        s1=s2;
        s2=s3;


        System.out.println(s1);
        System.out.println(s2);

    }
}
