package PatternPrograme;

public class Pattern5 {
    public static void main(String[] args) {

        int lines = 5;
        int stars = 5;
        int ch=1;
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < stars; j++) {
                System.out.print( " "+ch+" " );
            }
            System.out.println();
            ch++;

        }
    }
}
