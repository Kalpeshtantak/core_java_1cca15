package PatternPrograme;

import java.util.Scanner;

public class Pattern4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the line & Star no");
        int star=sc.nextInt();
        for (int i=1;i<=star;i++) {
            for (int j = 1; j <=star; j++) {
                if (j==1||i==1||j==star||i==star ||j==i ||(i+j-1)==star || (star/2+1)==i ||(star/2+1)==j)
                {
                    System.out.print(" * ");
                }
                else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
}
