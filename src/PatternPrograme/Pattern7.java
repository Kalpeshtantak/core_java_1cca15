package PatternPrograme;

public class Pattern7 {
    public static void main(String[] args) {
        int lines = 4;
        int stars = 4;
        int ch1 = 1;
        for (int i = 0; i < lines; i++) {
            int ch2 =ch1;
            for (int j = 0; j < stars; j++) {
                System.out.print(" " + ch2++ + " ");

            }
            System.out.println();
            ch1++;

        }

    }
}
