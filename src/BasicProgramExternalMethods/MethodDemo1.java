package BasicProgramExternalMethods;

public class MethodDemo1 {
    //Internal Method

    public static void main(String[] args) {
        System.out.println("Main Method");
        MethodDemo1 m=new MethodDemo1();
        m.Display();

    }
    //External Method
    public  void Display(){
        System.out.println("Display Method");
        Object o=new Object();
       o.gettype();
       o.info();

    }
public static void GetInfo(){
    System.out.println("Extra Method");
//    Display();
}
}
class Object {
    public static void gettype(){
        System.out.println("GEt type");
    }
    public static void info(){
        System.out.println("Info");
        gettype();
    }
}