package BasicProgramExternalMethods;

import java.util.Scanner;

public class Operation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(" 1.Multiplication  \n 2.Addition \n 3.Subtraction");
        int ch = sc.nextInt();
        System.out.println("Enter First No");
        double num1 = sc.nextDouble();
        System.out.println("Enter Your Second No ");
        double num2 = sc.nextDouble();

        if (ch == 1) {
            System.out.println("Multiplication=" + (num1 * num2));

        } else if (ch == 2) {
            System.out.println("Addition=" + (num1 + num2));
        } else if (ch == 3) {
            System.out.println("Subtraction=" + (num1 - num2));
        }else {
            System.out.println("Invalid Choice");
        }
    }
}