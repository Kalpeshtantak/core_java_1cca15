package BasicProgramExternalMethods;

import java.util.Scanner;

public class Demo3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Quantity");
        int Qty = sc.nextInt();
        System.out.println("Total Price");
        double Price = sc.nextDouble();
        TotalAmount(Qty,Price);
    }
    public static void TotalAmount(int Qty,double Price){
        double total=Qty*Price;
        System.out.println("Total Amount"+total);
        double DiscountedAmount;
        double finalAmount;

        if( total<500&&total>0){
             DiscountedAmount=0.05*total;
             finalAmount=total-DiscountedAmount;
             System.out.println(" Final Amount = "+finalAmount);}
         else if (total>500&&total<1000) {
             DiscountedAmount=0.1*total;
             finalAmount=total-DiscountedAmount;
            System.out.println(" Final Amount = "+finalAmount);}
         else if (total>1000) {
             DiscountedAmount=0.15*total;
             finalAmount=total-DiscountedAmount;
            System.out.println(" Final Amount = "+finalAmount);
        }

    }

    }

