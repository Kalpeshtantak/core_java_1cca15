package AbstractClass;

public abstract class Master {
    void test() {
        System.out.println("Test Method");
    }
    abstract void Display();
}
