package AbstractClass;

import java.util.Scanner;

public class MainApp4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Type");
        System.out.println("1:Manager \n 2: Watchman ");
        int ch= sc.nextInt();
        Employee e=null;
        if (ch==1){
            e=new Manager();
        } else if (ch==2) {
            e=new Watchman();
        }else {
            System.out.println("Invalid Choice");
        }
        e.GetDesignation();
        e.GetSalary();
    }
}
abstract  class Employee{
    abstract void GetDesignation();
    abstract void GetSalary();
}
class Manager extends Employee{
    void GetDesignation() {
        System.out.println(" Designation Is Manager ");
    }
    @Override
    void GetSalary() {
        System.out.println("Salary Is 450000");
    }
}
class Watchman extends Employee{
    @Override
    void GetDesignation() {
        System.out.println("Designation Is Watchman");
    }

    @Override
    void GetSalary() {
        System.out.println("Salary Is 400000");
    }
}