package AbstractClass;

public abstract class Demo {
    static int k=20;
    double d=35.25;
    abstract void test();
    void display(){
        System.out.println("Display Method");
}
static void info(){
    System.out.println(" Info Method ");
    }
    Demo(){
        System.out.println("It Is Constructor");
    }
    static {
        System.out.println("Static Block");
    }
    {
        System.out.println(" Non Static ");
    }
}

