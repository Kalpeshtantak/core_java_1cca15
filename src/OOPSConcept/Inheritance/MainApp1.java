package OOPSConcept.Inheritance;
public class MainApp1 {
    public static void main(String[] args) {
        Child C1=new Child();
        C1.test();
        C1.display();
    }
}
class Child extends Parent{
    void display(){
        System.out.println("Child Property");
    }
}
class Parent {
    void test(){
        System.out.println("Parent Property");
}
}