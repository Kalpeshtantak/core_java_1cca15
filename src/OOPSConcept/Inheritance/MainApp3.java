package OOPSConcept.Inheritance;
public class MainApp3 {
    public static void main(String[]args) {
      College l1=new College();
      l1.getinfo();
      College.s1.getType();
    }
}
class College {
    static Student s1=new Student();
    void getinfo(){
        System.out.println("College Name Is Sanjay Bhokare Institute");
        System.out.println("Placed In Miraj");
    }
}
class Student {
    void getType(){
        System.out.println("Student Only A Grade Allowed");
    }
}
