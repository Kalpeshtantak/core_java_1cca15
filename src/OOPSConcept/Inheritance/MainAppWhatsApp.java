package OOPSConcept.Inheritance;
public class MainAppWhatsApp extends  WhatsApp3{
    public static void main(String[] args) {
        WhatsApp3 W=new  WhatsApp3();
        W.Chatting();
        W.Calling();
        W.Payment();
    }
}
class WhatsApp1 {
    void Chatting() {
        System.out.println("You Can Chat With Friend");
    }
}
class WhatsApp2 extends WhatsApp1{
    void Calling(){
        System.out.println("You Can Make And Receive Call");

    }
}
class WhatsApp3 extends WhatsApp2{
    void Payment(){
        System.out.println("Now You Can Make Payment");

    }
        }

