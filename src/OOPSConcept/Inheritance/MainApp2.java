package OOPSConcept.Inheritance;

public class MainApp2 {
    public static void main(String[] args) {
        Laptop l1=new Laptop();
        l1.getinfo();
        Laptop.h1.getType();
    }
}
class Hdd {
    void getType(){
        System.out.println("Hdd Is SSD");
    }
}
class Laptop {
    static Hdd  h1=new Hdd();
    void getinfo(){
        System.out.println("Company Name Is Hp");
        System.out.println("Price Is 45000");
    }
}

