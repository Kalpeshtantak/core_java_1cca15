package PractiseOfProgram;

import java.util.Scanner;

public class SwitchCase2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter First No");
        double a= sc.nextDouble();
        System.out.println("Enter Second No");
        double b=sc.nextDouble();
        System.out.println("+");
        System.out.println("-");
        System.out.println("*");
        System.out.println("/");
        System.out.println("%");
        char c= sc.next().charAt(0);
        switch (c){
            case '+':
                System.out.println("Selected Addition="+(a+b));

            case '-':
                System.out.println("Selected Subtraction="+(a-b));

            case '*':
                System.out.println("Selected Multiplication="+(a*b));

            case '/':
                System.out.println("Selected Divide="+(a/b));

            case '%':
                System.out.println("Selected Mode="+(a%b));
            default:
                System.out.println("Invalid Choice");
        }
    }
}


