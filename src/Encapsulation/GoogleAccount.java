package Encapsulation;
//singleton Class
public class GoogleAccount {
    //static int count;
    static GoogleAccount g1;
    private  GoogleAccount(){
    }
    //Singletone Method
    static GoogleAccount login(){
        if(g1==null){
            g1=new GoogleAccount();
            System.out.println("Log In Successfully");
        }   else {
            System.out.println("Already logIn" );

        }
        return g1;
    }
    void AccessGmail(){
        System.out.println("Access Gmail");
    }
    void accessDrive(){
        System.out.println("Accessing Drive");
    }
}
