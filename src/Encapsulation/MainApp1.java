package Encapsulation;

public class MainApp1 {
    public static void main(String[] args) {
        Employee E1=new Employee();
        int id =E1.getEmpId();
        double Salary=E1.getEmpSalary();
        System.out.println(" Id = "+id);
        System.out.println(" Salary  = "+Salary);
        E1.setEmpId(201);
        E1.setEmpSalary(-35000);

        System.out.println(E1.getEmpSalary());
        System.out.println(E1.getEmpId());
    }
}
