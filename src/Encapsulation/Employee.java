package Encapsulation;
//java bean class
public class Employee {
    private int EmpId = 101;
    private double EmpSalary = 12000;

    public int getEmpId() {
        return EmpId;
    }

    public void setEmpId(int empId) {
        this.EmpId = empId;
    }

    public double getEmpSalary() {
        return EmpSalary;
    }

    public void setEmpSalary(double empSalary)
    {
              if (empSalary > 0) {
                this.EmpSalary = empSalary;
            } else {
                System.out.println(" Invalid Amount ");
            }
        }
}




