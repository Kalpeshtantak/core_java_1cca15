package String;

public class StringDemo1 {
    public static void main(String[] args) {
        String s= "Software Developer";
        System.out.println(s.length());
        System.out.println(s.charAt(12));
        System.out.println(s.indexOf('w'));
        System.out.println(s.lastIndexOf('e'));
        System.out.println(s.contains("wafre"));
        System.out.println(s.startsWith("aoftw"));
        System.out.println(s.endsWith("pr"));
        System.out.println(s.substring(2));
        System.out.println(s.substring(0,10));
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
    }
}
