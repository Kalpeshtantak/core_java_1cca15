package Interfaces;

public class Software extends FrontEnd implements BackEnd,Database {
    @Override
    public void DevelopeServerProgram(String Language) {
        System.out.println("Developing Server Program Using="+Language);
    }

    @Override
    public void DesignDatabase(String dbdeveloper) {
        System.out.println("Design Database="+dbdeveloper);
    }
}