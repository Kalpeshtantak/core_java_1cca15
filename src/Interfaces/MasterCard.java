package Interfaces;

public class MasterCard implements CreditCard{

    @Override
    public void getType() {
        System.out.println("Card Type Is Master Card");
    }

    @Override
    public void Withdraw(double amt) {
        System.out.println("Withdraw Amount Is  "+amt);
    }
}
