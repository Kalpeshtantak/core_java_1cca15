package Inheritance1;

public class MainApp1 {
    public static void main(String[] args) {
        System.out.println("Main Class");
        Child c1=new Child();
        c1.test();
        c1.display();
    }
}
class Parent {
    void test(){
        System.out.println("Parent Properties");
    }
}
class Child extends Parent {
    void display(){
        System.out.println("Child Property");
    }
}