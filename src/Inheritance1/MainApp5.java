package Inheritance1;

public class MainApp5 {
    public static void main(String[] args) {
        System.out.println("Main App");
        Sample1 S1=new Sample1();
    }
}
class Demo1{
    public Demo1(int a){
        System.out.println("SuperClass Constructor a value: "+a);
    }
}
class Sample1 extends Demo1{
//    Demo1 d;
    public Sample1(){
//       d= new Demo1(25);
        super(25);
        System.out.println("Subclass Constructor");

    }
}
