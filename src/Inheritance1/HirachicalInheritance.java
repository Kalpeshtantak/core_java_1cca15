package Inheritance1;

public class HirachicalInheritance {
    public static void main(String[] args) {
        System.out.println("Main Class Start");
        PEmployee P1=new PEmployee();
        CEmployee C1=new CEmployee();
        P1.GetInfo(1,35000);
        P1.Desingantion("Java Develpoer ");
        C1.GetInfo(2,40000);
        System.out.println("==============");
        C1.CDetails(24);
        System.out.println("Main Class End");
    }
}
class Employee{
    void GetInfo(int EmpId,double EmpSalary){
        System.out.println("Emp Id ="+EmpId);
        System.out.println("Emp Salary ="+EmpSalary);
    }
}
class PEmployee extends Employee{
    void Desingantion(String Desgn){
        System.out.println("Designation Is = "+Desgn);
    }
}
class CEmployee extends Employee{
    void CDetails(int Months){
        System.out.println("Contract Duration = "+Months);
    }
}