package Inheritance1;

public class MainApp2 {
    public static void main(String[] args) {
        WhatsApp3 W3=new WhatsApp3();
        W3.Chatting();
        W3.Call();
        W3.Payment();
    }
}
class WhatsApp1{
    void Chatting(){
        System.out.println("You can Chat Now ");
    }
}
class WhatsApp2 extends WhatsApp1{
    void Call(){
        System.out.println("You Can Make Received Call");
    }
}
class WhatsApp3 extends WhatsApp2{
    void Payment (){
        System.out.println("You can MAke Payment");
    }
}