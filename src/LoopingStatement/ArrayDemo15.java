package LoopingStatement;

public class ArrayDemo15{
    public static void main(String[] args) {
        String[][] data;
        data = new String[3][2];
        data[0][0] = "Maharashtra";
        data[0][1] = "Mumbai";
        data[1][0] = "Karnataka";
        data[1][1] = "Bengaluru";
        data[2][0] = "Gujarat";
        data[2][1] = "Surat";
        for (int a = 0; a < data.length; a++) {
            for (int c = 0; c < data[a].length; c++) {
                System.out.println(data[a][c]);
            }
            System.out.println();
        }
    }
}