package LoopingStatement;

public class ArrayDemo6 {
    public static void main(String[] args) {
        int[] arr1 ={20, 30, 40};
        int[] arr2 ={60, 70, 80};
        double[] sum =new double[arr1.length];
        for (int a = 0; a < arr1.length; a++) {
            sum[a] = arr1[a] + arr2[a];
            System.out.println(" arr1+arr2 = " + sum[a] + " ");
        }
    }
}
