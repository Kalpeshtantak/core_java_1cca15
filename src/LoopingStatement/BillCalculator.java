package LoopingStatement;

import java.util.Scanner;

public class BillCalculator {
    public void calculateBill(double[] amounts) {
        Scanner sc = new Scanner(System.in);
        double[] gstvalues = gstcalculation(amounts);
        //create new array to calculate total amount
        double[] totalAmounts = new double[amounts.length];
        //perform sum of amount & gst values
        for (int a = 0; a < amounts.length; a++) {
            totalAmounts[a] = amounts[a] + gstvalues[a];
        }
        double totalBillAmt = 0.0;
        double totalgstAmt = 0.0;
        double totalfinalAmt = 0.0;
        //sum of all array elements
        for (int a = 0; a < amounts.length; a++) {
            totalBillAmt += amounts[a];
            totalgstAmt += gstvalues[a];
            totalfinalAmt += totalAmounts[a];
        }
        //present final output for user
        System.out.println("BillAmt\t" + "GstAmt\t" + "Total");
        System.out.println("============");
        for (int a = 0; a < amounts.length; a++) {
            System.out.println("amounts[a]\t" + "gstValues[a]\t" + "totalfinalAmt");
        }
        System.out.println("========");
        System.out.println(totalBillAmt + "\t" + totalgstAmt + "\t" + totalfinalAmt);
    }

    public double[] gstcalculation(double[] amounts) {
        //create new array to stoare gst amount
        double[] gstAmounts = new double[amounts.length];
        for (int a = 0; a < amounts.length; a++) {
            if (amounts[a] < 500) {
                gstAmounts[a] = amounts[a] * 0.05;
            }//5%gst

            else {
                gstAmounts[a] = amounts[a] * 0.1; //10%gst


            }
        }
        //return gst array to calculationBillMethod
        return gstAmounts;

    }
}


