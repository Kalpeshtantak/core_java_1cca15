package LoopingStatement;
import java.util.*;
public class ArrayDemo3{
    public static void main(String[]args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Total No Of Courses");
        int size=sc.nextInt();
        String []courses=new String[size];
        System.out.println("Enter Course Name");
        for(int a=0;a<size;a++){
            courses[a]=sc.next();
        }
        System.out.println("Select Courses");
        for (int a=0;a<size;a++){
            System.out.println(courses[a]);
        }

    }
}
