package LoopingStatement;

public class ArrayDemo13 {
    public static void main(String[] args) {
        //2D Array
        int[][] data;
        data = new int[2][2];
        data[0][0] = 100;
        data[0][1] = 200;
        data[1][0] = 300;
        data[1][1] = 400;
        //rows
        //cols
        for (int a= 0; a < data.length; a++) {
            //cols
            for (int c=0; c< data.length; c++) {
                System.out.print(data[a][c] + "\t");
            }
            System.out.println();
        }
    }
}


