package LoopingStatement;
import java.util.*;
public class ForLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Start Point");
        int start=sc.nextInt();
        System.out.println("Enter End Point");
        int end=sc.nextInt();
        for(int a=start;a<=end;a++ ){
            if (a%2==0){
                System.out.println(a*a);
            }
        }
    }
}
