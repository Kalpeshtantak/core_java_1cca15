package LoopingStatement;

public class ArrayDemo9 {
    public static void main(String[] args) {
        int[]data=new int[3];
        data[0]=100;
        data[1]=200;
        data[2]=300;
        printArray(data);
    }
    static void printArray(int[]info){
        for(int a=0;a< info.length;a++){
            System.out.println(info[a]);
        }
    }
}
