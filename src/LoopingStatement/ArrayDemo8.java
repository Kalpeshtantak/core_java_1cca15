package LoopingStatement;
import java.util.*;
 class StoreManager {
    static String[] product = {"TV", "PROJECTOR", "MOBILE"};
    static double[] cost = {30000, 7300, 15000};
    static int[] stock = {50, 30, 100};


    void calculateBill(int choice, int qty) {
        boolean found = false;
        for (int a = 0; a < product.length; a++) {
            if (choice == a && qty <= stock[a]) {
                double total = qty * cost[a];
                stock[a] = qty;
                System.out.println("Total Bill" + total);
                found = true;
            }
        }
        if (!found)
            System.out.println("Product Not Found Or Else Out Of Stock");

    }

}
public class ArrayDemo8 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        boolean status=true;
        while(status){
            System.out.println("Select Product");
            System.out.println("0:TV\n1:PROJECTOR\n2:MOBILE\n3:Exit");
            int choice=sc.nextInt();
            System.out.println("Enter qty");
            int qty= sc.nextInt();
            StoreManager s1=new StoreManager();
            if (choice==0 || choice==1|| choice==2){
                s1.calculateBill(choice,qty);
            }
            else {
                System.out.println("Invalid");
                status=false;
            }
        }
    }
}
