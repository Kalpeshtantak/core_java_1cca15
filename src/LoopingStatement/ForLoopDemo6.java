package LoopingStatement;
import java.util.*;
public class ForLoopDemo6 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter No Of User");
        int count=sc.nextInt();
        for (int a=1;a<=count;a++){
            System.out.println("Enter UserName");
            String name=sc.next();
            System.out.println("Welcome"+name);
        }
    }
}
