package LoopingStatement;
import java.util.*;
public class WhileLoopDemo1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        while (n >= 0) {
            System.out.println("Enter No");
             n = sc.nextInt();
            System.out.println(n);
        }

        System.out.println("Entered Negative value");
    }
}
