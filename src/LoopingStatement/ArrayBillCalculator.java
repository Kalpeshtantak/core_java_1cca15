package LoopingStatement;

import java.util.Scanner;

public class ArrayBillCalculator {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BillCalculator b1=new BillCalculator();
        System.out.println("Enter Total No Of Bill");
        int count=sc1.nextInt();
        double []amounts=new double[count];
        System.out.println("Enter Bill Amoount");
        //accept bill amount from users
        for (int a=0; a<count;a++){
            amounts[a]= sc1.nextDouble();
        }
        //call method from Business class

        b1.calculateBill(amounts);
    }
}
