package LoopingStatement;
import java.util.*;
public class ForLoopDemo5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Start Point ");
        int start = sc.nextInt();
        System.out.println("Enter End Point");
        int end = sc.nextInt();
        double sum = 0;
        for (double a = start; a <= end; a++) {
            if (a % 2 == 0) {
                sum += a;
            }

        }
        System.out.println("Sum=" + sum);
    }
}
