package MethodOverloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Student S1= new Student();
        System.out.println("Select Search Criteria");
        System.out.println("1:Search By Name");
        System.out.println("2:Search By Contact");
        int ch= sc.nextInt();
        if (ch==1){
            System.out.println("Enter Name");
            String name=sc.next();
            S1.search(name);
        } else if (ch==2) {
            System.out.println("Enter Contact");
            int Contact= sc.nextInt();
            S1.search(Contact);
        }else {
            System.out.println("Invalid Choice");
        }
    }
    }
class Student{
    String Name="Kalpesh";
    int Contact=1234;
    void search(String StudName){
        if (StudName.equalsIgnoreCase(Name)){
            System.out.println("Student Name Is = "+Name);
            System.out.println("Student Contact No ="+Contact);}
        else {
            System.out.println("Record Not Found");
        }
    }
    void search(int ContactNo){
        if (ContactNo==Contact){
            System.out.println("Student Name Is"+Name);
            System.out.println("Student Contact"+Contact);
        }else{
            System.out.println(" Record Not Found ");
        }
    }
}
